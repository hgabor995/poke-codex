# Statement of work ( SOW ):

-   [x] During your work use the https://pokeapi.co/ REST API.
-   [x] List the different types of pokemons on the main screen. ( Use a dropdown menu or picker )
-   [x] When one of the types is selected list all the pokemon names which are assigned to this type. Include a searchbar on this screen, to able to search pokemons by name.
-   [x] Upon selection of a pokemon, navigate to a profile card where you show some details about the selected pokemon.
-   [x] The detailed profile card needs to include: a picture of the pokemon, the name, weight, height, not hidden abilites.
-   [x] On the same screen implement a 'catch' button, on click, the pokemon should be catched. You will need to mark the catched pokemons on the list. (exmaple:green border, whatever...). When a pokemon is catched, the button should turn to'release', if you want to release them back to the wild.
-   [x] Implement checkbox next to the searchbar to list only your catched pokemons.
-   [x] Add some navigation to go back on the webpage, and do not fetch the data again
-   [x] Use a loader / spinner while fetching data. (Loading…).

## Requirements:

-   [x] ReactJS with Typescript and Redux state management.
-   [ ] For every component decide wisely if you need a class based component with local states, or a functional component with hooks. (If you need help let me know)
-   [x] Usage of ES6
-   [ ] Easy to read, DRY ( Do Not Repeat Yourself ) and well structured component based code.
-   [ ] Documentation with JSDocs, unless the code documents itself
-   [x] You can use any css library of your like, or you can write the whole on your own.
