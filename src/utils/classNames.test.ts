import classNames from './classNames';

describe('Tests for `classNames` util:', () => {
    test('without parameter:', cb => {
        expect(classNames()).toEqual('');
        cb();
    });

    test('with falsy and true parameters:', cb => {
        expect(classNames(typeof 42 === 'string', null, void 0, true)).toEqual('');
        cb();
    });

    test('with 1 string parameter:', cb => {
        expect(classNames('container')).toEqual('container');
        cb();
    });

    test('with 2 string parameters:', cb => {
        expect(classNames('column', 'col-6')).toEqual('column col-6');
        cb();
    });

    test('with a simple object:', cb => {
        expect(classNames({ loading: true })).toEqual('loading');
        cb();
    });

    test('with a complex object:', cb => {
        expect(
            classNames({
                loading: true,
                disabled: false,
                'number-value': 42,
                zero: 0,
                text: 'not empty',
                empty: '',
                'allow-any-object': { content: 'is ignored' },
                null: null,
                undefined: undefined,
            }),
        ).toEqual('loading number-value text allow-any-object');
        cb();
    });

    test('with multiple parameter', cb => {
        expect(getButtonClassNames({ resize: 'sm', loading: false })).toEqual('btn btn-sm');
        cb();
    });
});

interface ButtonProps {
    resize?: 'sm' | 'lg';
    className?: string;
    loading?: boolean;
}

function getButtonClassNames(props: ButtonProps): string {
    return classNames(
        'btn',
        {
            [`btn-${props.resize}`]: props.resize,
            loading: props.loading,
        },
        props.className,
    );
}
