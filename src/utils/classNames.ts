// Copyright: https://github.com/JedWatson/classnames
// I didn't copy the code, but the idea is theirs.
// Not full, a just "demo" version :)

export default function classNames(...classes: (string | boolean | null | undefined | Record<string, any>)[]) {
    return classes
        .filter((value): value is string | Record<string, any> => !!value && value !== true)
        .map(value => (typeof value === 'string' ? value : objectToClassNames(value)))
        .join(' ');
}

function objectToClassNames(obj: Record<string, any>) {
    return Object.keys(obj)
        .filter(key => !!obj[key])
        .join(' ');
}
