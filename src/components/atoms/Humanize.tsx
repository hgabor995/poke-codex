import React from 'react';

export interface HumanizeProps {
    children?: string;
}

/**
 * Replace cebab-case text to text separated Pascal Case.
 */
export default function Humanize(props: HumanizeProps) {
    if (!props.children) {
        return null;
    }
    const name = props.children
        .split('-')
        .map(value => `${value.charAt(0).toUpperCase()}${value.slice(1)}`)
        .join(' ');
    return <>{name}</>;
}
