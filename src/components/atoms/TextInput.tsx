import React from 'react';
import Icon, { IconType } from './Icon';

export interface TextInputProps {
    icon?: IconType;
    postfixIcon?: boolean;

    value?: string;
    onChange?: (value?: string) => void;
}

export default function TextInput(props: TextInputProps) {
    const { onChange } = props;

    const handleChange: React.ChangeEventHandler<HTMLInputElement> = React.useCallback(
        event => {
            if (onChange) {
                onChange(event.target.value);
            }
        },
        [onChange],
    );

    const inputProps: React.InputHTMLAttributes<HTMLInputElement> = {
        className: 'form-input',
        type: 'text',
        id: 'filter-input',
        placeholder: 'Search',
        value: props.value,
        onChange: handleChange,
    };

    if (!props.icon) {
        return <input {...inputProps} />;
    }

    return (
        <div className={props.postfixIcon ? 'has-icon-right' : 'has-icon-left'}>
            <input {...inputProps} />
            <Icon type={props.icon} className="form-icon" />
        </div>
    );
}
