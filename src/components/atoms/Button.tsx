import React from 'react';
import classNames from '../../utils/classNames';

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    resize?: 'sm' | 'lg';
    className?: string;
    loading?: boolean;
    disabled?: boolean;
}

export default function Button(props: ButtonProps) {
    const className = classNames(
        'btn',
        { [`btn-${props.resize}`]: props.resize, loading: props.loading },
        props.className,
    );

    return <button {...props} className={className} disabled={props.disabled} />;
}
