import React from 'react';

export interface LoadingProps {
    lg?: boolean;
}

export default function Loading(props: LoadingProps) {
    return <div className={props.lg ? 'loading loading-lg' : 'loading'} />;
}
