import React from 'react';

export interface CheckboxProps {
    value?: boolean;
    onChange?: (value?: boolean) => void;

    children: React.ReactNode;
}

export default function Checkbox(props: CheckboxProps) {
    const { onChange } = props;

    const handleChange: React.ChangeEventHandler<HTMLInputElement> = React.useCallback(
        event => {
            if (onChange) {
                onChange(event.target.checked);
            }
        },
        [onChange],
    );

    return (
        <label className="form-checkbox form-inline">
            <input type="checkbox" checked={props.value} onChange={handleChange} />
            <i className="form-icon" /> {props.children}
        </label>
    );
}
