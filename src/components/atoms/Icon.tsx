import React from 'react';
import classNames from '../../utils/classNames';

type NavigationIcons =
    | 'arrow-up'
    | 'arrow-right'
    | 'arrow-down'
    | 'arrow-left'
    | 'upward'
    | 'forward'
    | 'downward'
    | 'back'
    | 'caret'
    | 'menu'
    | 'apps'
    | 'more-horiz'
    | 'more-vert';

type ActionIcons =
    | 'resize-horiz'
    | 'resize-vert'
    | 'plus'
    | 'minus'
    | 'cross'
    | 'check'
    | 'stop'
    | 'shutdown'
    | 'refresh'
    | 'search'
    | 'flag'
    | 'bookmark'
    | 'edit'
    | 'delete'
    | 'share'
    | 'download'
    | 'upload'
    | 'copy';

type ObjectIcons = 'mail' | 'people' | 'message' | 'photo' | 'time' | 'location' | 'link' | 'emoji';

export type IconType = NavigationIcons | ActionIcons | ObjectIcons;

export interface IconProps {
    type: IconType;
    className?: string;
}

export default function Icon(props: IconProps) {
    const { type, ...rest } = props;

    let className = classNames('icon', `icon-${type}`, props.className);
    return <i {...rest} className={className} />;
}
