import React from 'react';

export interface HeroProps {
    title: React.ReactNode;
    example?: React.ReactNode;
}

export default function Hero(props: HeroProps) {
    return (
        <div className="hero text-center">
            <div className="hero-body">
                <h1>{props.title}</h1>
                <p>{props.example}</p>
            </div>
        </div>
    );
}
