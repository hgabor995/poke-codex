import React from 'react';
import Icon, { IconType } from '../atoms/Icon';

export interface EmptyStateProps {
    icon: IconType;
    title: React.ReactNode;
    subtitle: React.ReactNode;
}

export default function EmptyState(props: EmptyStateProps) {
    return (
        <div className="empty">
            <div className="empty-icon">
                <Icon type={props.icon} className="icon-3x" />
            </div>
            <p className="empty-title h5">{props.title}</p>
            <p className="empty-subtitle">{props.subtitle}</p>
        </div>
    );
}
