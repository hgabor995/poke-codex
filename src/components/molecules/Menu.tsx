import React from 'react';
import MenuItem, { MenuItemProps } from './MenuItem';

export interface MenuProps {
    children: React.ReactElement<MenuItemProps, typeof MenuItem> | React.ReactElement<MenuItemProps, typeof MenuItem>[];
}

export default function Menu(props: MenuProps) {
    return <ul className="menu">{props.children}</ul>;
}
