import React from 'react';

export interface MenuItemProps {
    children: React.ReactNode;
    onClick?: () => void;
    selected?: boolean;
}

export default function MenuItem(props: MenuItemProps) {
    return (
        <li className="menu-item text-left" onClick={props.onClick}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a className={props.selected ? 'active' : undefined} href="#">
                {props.children}
            </a>
        </li>
    );
}
