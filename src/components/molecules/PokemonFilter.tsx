import React from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { FilterActions, selectFilter } from '../../features/filter/filterSlice';
import Checkbox from '../atoms/Checkbox';
import TextInput from '../atoms/TextInput';

export default function PokemonFilter() {
    const filter = useAppSelector(selectFilter);
    const dispatch = useAppDispatch();

    const onSearchChange = React.useCallback((value?: string) => dispatch(FilterActions.search(value)), [dispatch]);
    const onCacheChange = React.useCallback((value?: boolean) => dispatch(FilterActions.caught(value)), [dispatch]);

    return (
        <div className="columns">
            <div className="column col-9 text-right">
                <Checkbox value={filter.caught} onChange={onCacheChange}>
                    Show only the caught
                </Checkbox>
            </div>
            <div className="column col-3">
                <TextInput icon="search" postfixIcon value={filter.search} onChange={onSearchChange} />
            </div>
        </div>
    );
}
