import React from 'react';
import Button from '../atoms/Button';
import Icon from '../atoms/Icon';
import Menu, { MenuProps } from './Menu';

export interface DropdownButtonProps {
    title: React.ReactNode;
    children?: React.ReactElement<MenuProps, typeof Menu>;
    loading?: boolean;
    disabled?: boolean;
}

export default function DropdownButton(props: DropdownButtonProps) {
    return (
        <div className="dropdown" tabIndex={0}>
            <Button className="dropdown-toggle" loading={props.loading} disabled={props.disabled}>
                {props.title} <Icon type="caret" />
            </Button>
            {props.children}
        </div>
    );
}
