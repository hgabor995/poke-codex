import React from 'react';
import Hero from '../molecules/Hero';
import PokemonModal from '../organisms/PokemonModal';
import PokemonTable from '../organisms/PokemonTable';
import TypeDropdown from '../organisms/TypeDropdown';

function App() {
    return (
        <>
            <Hero title="Poke Codex" example={<TypeDropdown />} />
            <PokemonTable />
            <PokemonModal />
        </>
    );
}

export default App;
