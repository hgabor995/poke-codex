import React from 'react';
import { Pokemon } from '../../api/models/Pokemon';
import PokeAPI from '../../api/PokeAPI';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { CatchingActions, selectCatchingList } from '../../features/catching/catchingSlice';
import { PokemonActions, selectCurrentPokemon } from '../../features/pokemon/pokemonSlice';
import Button from '../atoms/Button';
import Humanize from '../atoms/Humanize';
import Loading from '../atoms/Loading';

export default function PokemonModal() {
    const selectedPokemon = useAppSelector(selectCurrentPokemon);
    const catchingList = useAppSelector(selectCatchingList);
    const dispatch = useAppDispatch();

    const caught = React.useMemo(() => catchingList.includes(selectedPokemon), [catchingList, selectedPokemon]);

    const className = selectedPokemon ? 'modal active' : 'modal';

    const [pokemon, setPokemon] = React.useState<Pokemon>();

    React.useEffect(() => {
        if (selectedPokemon) {
            PokeAPI.getPokemonByName(selectedPokemon).then(setPokemon);
        } else {
            setPokemon(undefined);
        }
    }, [selectedPokemon]);

    const onClose = React.useCallback(() => {
        dispatch(PokemonActions.select(undefined));
    }, [dispatch]);

    const onCatchToggle = React.useCallback(() => {
        if (selectedPokemon) {
            dispatch(CatchingActions[caught ? 'release' : 'catch'](selectedPokemon));
        }
    }, [caught, dispatch, selectedPokemon]);

    return (
        <div className={className} id="modal-id">
            <span className="modal-overlay" aria-label="Close" onClick={onClose} />
            <div className="modal-container">
                <div className="modal-header">
                    <button className="btn btn-clear float-right" aria-label="Close" onClick={onClose} />
                    {selectedPokemon && (
                        <h4>
                            Profile Card for{' '}
                            <strong className="text-primary">
                                <Humanize>{selectedPokemon}</Humanize>
                            </strong>
                        </h4>
                    )}
                </div>
                <div className="modal-body">
                    {pokemon ? (
                        <>
                            <div className="text-center">
                                <img
                                    src={
                                        pokemon.sprites.other['official-artwork'].front_default ||
                                        pokemon.sprites.front_default
                                    }
                                    alt={selectedPokemon}
                                    width={160}
                                />
                            </div>
                            <div className="divider text-center" data-content="DETAILS" />
                            <dl>
                                <dt>Height:</dt>
                                <dd>{pokemon.height}</dd>
                                <dt>Weight:</dt>
                                <dd>{pokemon.weight}</dd>
                                <dt>Abilities:</dt>
                                <dd>
                                    {pokemon.abilities
                                        .filter(value => !value.is_hidden)
                                        .map(value => (
                                            <span className="chip">
                                                <Humanize>{value.ability.name}</Humanize>
                                            </span>
                                        ))}
                                </dd>
                            </dl>
                        </>
                    ) : (
                        <Loading lg />
                    )}
                </div>
                <div className="modal-footer">
                    <Button onClick={onCatchToggle}>{caught ? 'RELEASE' : 'CATCH'}</Button>
                </div>
            </div>
        </div>
    );
}
