import React from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { selectCatchingList } from '../../features/catching/catchingSlice';
import { selectFilter } from '../../features/filter/filterSlice';
import fetchAllPokemonByType from '../../features/pokemon/fetchAllPokemonByType';
import { PokemonActions, selectPokemonFetching, selectPokemonList } from '../../features/pokemon/pokemonSlice';
import { selectCurrentType } from '../../features/type/typeSlice';
import Button from '../atoms/Button';
import Humanize from '../atoms/Humanize';
import Icon from '../atoms/Icon';
import Loading from '../atoms/Loading';
import EmptyState from '../molecules/EmptyState';
import PokemonFilter from '../molecules/PokemonFilter';
import Table from './Table';
import TableColumn from './TableColumn';

export default function PokemonTable() {
    const currentType = useAppSelector(selectCurrentType);
    const loading = useAppSelector(selectPokemonFetching);
    const pokemonList = useAppSelector(selectPokemonList);
    const filter = useAppSelector(selectFilter);
    const catchingList = useAppSelector(selectCatchingList);
    const dispatch = useAppDispatch();

    React.useEffect(() => {
        if (currentType) {
            dispatch(fetchAllPokemonByType(currentType));
        }
    }, [currentType, dispatch]);

    const filteredPokemonList = React.useMemo(() => {
        return pokemonList
            .filter(value => !filter.caught || catchingList.includes(value))
            .filter(value => value.includes(filter.search))
            .map(name => ({ name, caught: catchingList.includes(name) }));
    }, [catchingList, filter.caught, filter.search, pokemonList]);

    if (!currentType) {
        return (
            <EmptyState
                icon="stop"
                title="No selected Type"
                subtitle="Select a type to list all the Pokemon which are assigned to the type."
            />
        );
    }

    if (loading) {
        return <Loading lg />;
    }

    return (
        <>
            <PokemonFilter />
            <Table dataSource={filteredPokemonList} noDataSubtitle="No pokemon can be found with this type.">
                <TableColumn title="Name" dataIndex="name" render={renderName} />
                <TableColumn title="Caught" dataIndex="caught" render={renderCaught} />
                <TableColumn
                    title="View Details"
                    dataIndex="name"
                    className="text-right"
                    render={renderViewDetailsButton}
                />
            </Table>
        </>
    );
}

// UTILS

function renderName(name: string) {
    return <Humanize>{name}</Humanize>;
}

function renderCaught(caught: boolean) {
    return caught ? <Icon type="check" className="text-success" /> : <Icon type="cross" className="text-error" />;
}

function renderViewDetailsButton(name: string) {
    return <ViewDetailButton name={name} />;
}

function ViewDetailButton(props: Record<'name', string>) {
    const dispatch = useAppDispatch();
    const onCLick = React.useCallback(() => dispatch(PokemonActions.select(props.name)), [dispatch, props.name]);
    return (
        <Button resize="sm" onClick={onCLick} className="tooltip tooltip-left" data-tooltip="View details">
            <Icon type="more-horiz" />
        </Button>
    );
}
