import React from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import fetchAllType from '../../features/type/fetchAllType';
import { selectTypeFetching, selectTypeList } from '../../features/type/typeSlice';
import DropdownButton from '../molecules/DropdownButton';
import Menu from '../molecules/Menu';
import TypeDropdownMenuItem from './TypeDropdownMenuItem';

export default function TypeDropdown() {
    const loading = useAppSelector(selectTypeFetching);
    const types = useAppSelector(selectTypeList);
    const dispatch = useAppDispatch();

    React.useEffect(() => {
        dispatch(fetchAllType());
    }, [dispatch]);

    const menuItems = React.useMemo(() => types.map(type => <TypeDropdownMenuItem key={type} type={type} />), [types]);

    return (
        <DropdownButton title="Select a type" loading={loading}>
            <Menu>{menuItems}</Menu>
        </DropdownButton>
    );
}
