import React from 'react';

export interface TableColumnProps<T = any> {
    title: React.ReactNode;
    dataIndex: keyof T;
    render: (text: T[keyof T], dataIndex: keyof T, value: T) => React.ReactNode;
    className?: string;
}

export default function TableColumn<T = any>(_props: TableColumnProps<T>) {
    return null;
}
