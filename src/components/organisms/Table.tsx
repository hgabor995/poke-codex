import React from 'react';
import classNames from '../../utils/classNames';
import EmptyState from '../molecules/EmptyState';
import TableColumn, { TableColumnProps } from './TableColumn';

type ColumnElement<T> = React.ReactElement<TableColumnProps<T>, typeof TableColumn>;

export interface TableProps<T = any> {
    dataSource?: T[];
    noDataSubtitle: React.ReactNode;

    className?: string;
    children: ColumnElement<T> | ColumnElement<T>[];
}

export default function Table<T = any>(props: TableProps<T>) {
    const children = Array.isArray(props.children) ? props.children : [props.children];

    const headColumns = renderHeaderRowCells(children);

    const body = renderBody(children, props.dataSource);

    return (
        <>
            <table className={classNames('table', props.className)}>
                <thead>
                    <tr>{headColumns}</tr>
                </thead>
                <tbody>{body}</tbody>
            </table>
            {!props.dataSource?.length && <EmptyState icon="stop" title="No Data" subtitle={props.noDataSubtitle} />}
        </>
    );
}

function renderHeaderRowCells<T>(children: ColumnElement<T>[]) {
    return children.map((child, key) => (
        <th key={key} className={child.props.className}>
            {child.props.title}
        </th>
    ));
}

function renderBody<T>(children: ColumnElement<T>[], dataSource?: T[]) {
    return dataSource?.map((data, key) => <tr key={key}>{renderBodyRowCells(children, data)}</tr>);
}

function renderBodyRowCells<T>(children: ColumnElement<T>[], data: T) {
    return children.map((child, key) => {
        const text = data[child.props.dataIndex];
        return (
            <td key={key} className={child.props.className}>
                {child.props.render(text, child.props.dataIndex, data) || text}
            </td>
        );
    });
}
