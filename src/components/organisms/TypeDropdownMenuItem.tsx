import React from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { selectCurrentType, TypeActions } from '../../features/type/typeSlice';
import Humanize from '../atoms/Humanize';
import MenuItem from '../molecules/MenuItem';

export interface TypeDropdownMenuItemProps {
    type: string;
}

export default function TypeDropdownMenuItem(props: TypeDropdownMenuItemProps) {
    const currentType = useAppSelector(selectCurrentType);
    const dispatch = useAppDispatch();

    const onClick = React.useCallback(() => dispatch(TypeActions.select(props.type)), [dispatch, props.type]);

    return (
        <MenuItem onClick={onClick} selected={currentType === props.type}>
            <Humanize>{props.type}</Humanize>
        </MenuItem>
    );
}
