import { NamedAPIResource } from './NamedAPIResource';

export interface NamedAPIResourceList {
    count: number;
    next: string;
    previous: string;
    results: NamedAPIResource[];
}
