import { NamedAPIResource } from './NamedAPIResource';

export interface TypePokemon {
    pokemon: NamedAPIResource;
}
