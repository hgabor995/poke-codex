import { TypePokemon } from './TypePokemon';

export interface Type {
    pokemon: TypePokemon[];
}
