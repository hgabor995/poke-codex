import { NamedAPIResource } from './NamedAPIResource';

export interface PokemonAbility {
    is_hidden: boolean;
    ability: NamedAPIResource;
}
