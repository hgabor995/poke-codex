import { PokemonAbility } from './PokemonAbility';
import { PokemonSprites } from './PokemonSprites';

export interface Pokemon {
    height: number;
    weight: number;
    abilities: PokemonAbility[];
    sprites: PokemonSprites;
}
