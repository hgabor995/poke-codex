import { BaseAPI } from './APIUtils';
import { NamedAPIResourceList } from './models/NamedAPIResourceList';
import { Pokemon } from './models/Pokemon';
import { Type } from './models/Type';

export default class PokeAPI extends BaseAPI {
    public static getAllType() {
        return this.get<NamedAPIResourceList>('/type');
    }

    public static getPokemonByType(type: string) {
        return this.get<Type>(`/type/${type}`).then(response => response.pokemon);
    }

    public static getPokemonByName(name: string) {
        return this.get<Pokemon>(`/pokemon/${name}`);
    }
}
