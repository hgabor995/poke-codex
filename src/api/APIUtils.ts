export class BaseAPI {
    protected static BASE_URL = 'https://pokeapi.co/api/v2';

    protected static get<T>(endpoint: string): Promise<T> {
        return fetch(`${this.BASE_URL}${endpoint}`).then(value => value.json());
    }
}
