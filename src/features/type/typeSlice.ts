import createFetchWithSelectSlice from '../../app/createFetchWithSelectSlice';
import { RootState } from '../../app/store';
import fetchAllType from './fetchAllType';

const typeSlice = createFetchWithSelectSlice('type', fetchAllType);

export const TypeActions = typeSlice.actions;

export const selectTypeFetching = (state: RootState) => state.type.fetching;
export const selectTypeList = (state: RootState) => state.type.list;
export const selectCurrentType = (state: RootState) => state.type.list[state.type.current];

export default typeSlice.reducer;
