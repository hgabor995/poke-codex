import { createAsyncThunk } from '@reduxjs/toolkit';
import PokeAPI from '../../api/PokeAPI';

const fetchAllType = createAsyncThunk('type/fetchAll', async () => {
    const response = await PokeAPI.getAllType();
    return response.results.map(value => value.name);
});

export default fetchAllType;
