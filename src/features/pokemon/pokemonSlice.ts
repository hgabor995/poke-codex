import createFetchWithSelectSlice from '../../app/createFetchWithSelectSlice';
import { RootState } from '../../app/store';
import fetchAllPokemonByType from './fetchAllPokemonByType';

const pokemonSlice = createFetchWithSelectSlice('pokemon', fetchAllPokemonByType);

export const PokemonActions = pokemonSlice.actions;

export const selectPokemonFetching = (state: RootState) => state.pokemon.fetching;
export const selectPokemonList = (state: RootState) => state.pokemon.list;
export const selectCurrentPokemon = (state: RootState) => state.pokemon.list[state.pokemon.current];

export default pokemonSlice.reducer;
