import { createAsyncThunk } from '@reduxjs/toolkit';
import PokeAPI from '../../api/PokeAPI';

const fetchAllPokemonByType = createAsyncThunk('pokemon/fetchAllByType', async (type: string) => {
    const response = await PokeAPI.getPokemonByType(type);
    return response.map(value => value.pokemon.name);
});

export default fetchAllPokemonByType;
