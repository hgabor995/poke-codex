import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface FilterState {
    search: string;
    caught: boolean;
}

const initialState: FilterState = {
    search: '',
    caught: false,
};

const filterSlice = createSlice({
    name: 'filter',
    initialState,
    reducers: {
        search: (state, action: PayloadAction<string | undefined>) => {
            state.search = action.payload || '';
        },
        caught: (state, action: PayloadAction<boolean | undefined>) => {
            state.caught = action.payload || false;
        },
    },
});

export const FilterActions = filterSlice.actions;

export const selectFilter = (state: RootState) => state.filter;

export default filterSlice.reducer;
