import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface CatchingState {
    list: string[];
}

const initialState: CatchingState = {
    list: [],
};

const catchingSlice = createSlice({
    name: 'catching',
    initialState,
    reducers: {
        catch: (state, action: PayloadAction<string>) => {
            state.list = [...state.list, action.payload];
        },
        release: (state, action: PayloadAction<string>) => {
            const idx = state.list.indexOf(action.payload);
            if (idx !== -1) {
                state.list = [...state.list.slice(0, idx), ...state.list.slice(idx + 1)];
            }
        },
    },
});

export const CatchingActions = catchingSlice.actions;

export const selectCatchingList = (state: RootState) => state.catching.list;

export default catchingSlice.reducer;
