import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'spectre.css/dist/spectre-icons.min.css';
import 'spectre.css/dist/spectre.min.css';
import store from './app/store';
import App from './components/pages/App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root'),
);

reportWebVitals(console.log);
