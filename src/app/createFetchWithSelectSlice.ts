import { AsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface FetchWithSelectState {
    fetching: boolean;
    list: string[];
    error?: any;
    current: number;
}

const initialState: FetchWithSelectState = {
    fetching: false,
    list: [],
    current: -1,
};

export default function createFetchWithSelectSlice<ThunkArg>(name: string, thunk: AsyncThunk<string[], ThunkArg, {}>) {
    return createSlice({
        name,
        initialState,
        reducers: {
            select: (state, action: PayloadAction<string | undefined>) => {
                state.current = action.payload ? state.list.indexOf(action.payload) : -1;
            },
        },
        extraReducers(builder) {
            builder
                .addCase(thunk.pending, state => {
                    state.fetching = true;
                    state.list = [];
                    state.error = undefined;
                })
                .addCase(thunk.fulfilled, (state, action) => {
                    state.fetching = false;
                    state.list = action.payload;
                })
                .addCase(thunk.rejected, (state, action) => {
                    state.fetching = false;
                    state.error = action.error.message;
                });
        },
    });
}
