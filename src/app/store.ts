import { configureStore } from '@reduxjs/toolkit';
import catchingSlice from '../features/catching/catchingSlice';
import filterSlice from '../features/filter/filterSlice';
import pokemonSlice from '../features/pokemon/pokemonSlice';
import typeSlice from '../features/type/typeSlice';

const store = configureStore({
    reducer: {
        type: typeSlice,
        pokemon: pokemonSlice,
        filter: filterSlice,
        catching: catchingSlice,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
